# Laser

## Description

The animal-based monitoring  survey method is the reference for data acquisition on livestock performances (Poivey et al., 1981; Faugère and Faugère, 1986; Landais and Faugère, 1986; Tillard et al., 1997).

The method consists in following a sample of herds during a predefined period, of which whole or part of the animals are identified individually, generally using ear-tags. Enumerators regularly visit the herds, for instance every fifteen or thirty days. For demography, they count at each visit the animals present in the herds and record all the events having taken place between two successive visits (parturitions, deaths, sales, purchases, etc.).

Animal-based monitoring generate precise and reliable data (Lesnoff, 2013). They are well adapted to establish precise technical reference frames of the productivity of breeds or livestock farming systems or to implement field experimentations in rural areas to quantify the impact of interventions or innovations.

Animal-based monitoring implies repeated measurements on the same animal, which require a particular organization of the data collection, management and analysis. Laser can manage data of various natures:  demography (reproduction, mortality, exit and entry of animals in the herds), productions (milk, ponderal growth, body state), pathologies (symptoms, serologies), artificial inseminations and all kinds of zootechnical or medical interventions delivered on the level of the animal or the herd.

CIRAD has long experience of this type of survey. The first methodological work was carried out on bovines in the North of the Ivory Coast in the years 1970. This work was finalized in Senegal at the beginning of the years 1980 by the development of the PANURGE system (Faugère and Faugère, 1993), used until the end of the years 1990 in several countries (Senegal, Ivory Coast, Burkina Faso, Brasil). Other systems were proposed and used in Cameroun, Chad and Niger (PIKBEU, Planchenault, 1990). At the time of major updates of available commercial information systems, a broad reflection was carried out from 1995 to 1999 on the structure of the data bases necessary for animal-based monitoring in ruminant livestock herds (Lancelot et al., 1998). This work led to the development of the Laser software (Juanès and Lancelot, 1999), whose field applications provided many scientific publications. Then in 2016, Laser has been redesigned, still using Microsoft Access©. 

Take a look at this short movie which is an example of the implementation of cattle monitoring at Kianjasoa in Madagascar as part of a project called [ECLIPSE](https://umr-selmet.cirad.fr/en/projet-scientifique/nos-projets/eclipse). One of the aims of this project was to better describe the zootechnical potential of herds in a harsh environment.

[![laser monitoring](https://img.youtube.com/vi/Z8CGW_z42M8/mqdefault.jpg)](https://www.youtube.com/embed/Z8CGW_z42M8?si=GDcBqLhzHy4IuHKc "Laser monitoring")



The Laser method consists of the following resources:

- [Guide to getting started](https://gitlab.cirad.fr/selmet/livtools/laser/-/blob/master/doc/LASER_Introduction_v1.2.pdf)
- [Handbook](https://gitlab.cirad.fr/selmet/livtools/laser/-/blob/master/doc/laser_manual_v3.1.pdf)
- [Guide to conduct Laser survey](https://gitlab.cirad.fr/selmet/livtools/laser/-/blob/master/doc/LASER_Guide_V3.1.pdf)
- [Field questionnaires](https://gitlab.cirad.fr/selmet/livtools/laser/-/tree/master/quest)
- [Data collecting software](https://gitlab.cirad.fr/selmet/livtools/laser/-/tree/master/soft/Laser.accde)
- [Empty Laser database to start entering data](https://gitlab.cirad.fr/selmet/livtools/laser/-/tree/master/soft/data_Laser_empty.accdb)
- [R package laserdemog](https://gitlab.cirad.fr/selmet/livtools/laserdemog)


## Bibliography

[Faugère O., Faugère, B. 1986](https://doi.org/10.19182/remvt.8608). Suivi de troupeaux et contrôle de performances individuelles des petits ruminants en milieu traditionnel africain. Aspects méthodologiques, Rev. Elev. Méd. vét. Trop., vol. 39, no 1, p. 29‐40, 1986.

[Landais E., Faugère O. 1986](https://agritrop.cirad.fr/437041/) Un modèle illustré de système d’investigation pour l’étude pluridisciplinaire des systèmes d’élevage en milieu traditionel, Cah. Rech. Dév., vol. 25, p. 75‐94.

[Lesnoff, M. 2013](https://doi.org/10.19182/remvt.10142). Méthodes d'enquête pour l'estimation des taux démographiques des cheptels de ruminants domestiques tropicaux. Synthèse, limites et perspectives. Revue d'Elevage et de Médecine Vétérinaire des Pays Tropicaux, 66 (2) : pp. 57-67.

[Poivey, J. P., J. L. Seitz, J. L., Landais E. 1981](https://doi.org/10.19182/remvt.8273). Finalités et aspects méthodologiques d’un système informatisé de suivi individuel des animaux dans les élevages bovins villageois du Nord de la Côte d’Ivoire, Rev. Elev. Méd. vét. Trop., vol. 34, no 2, p. 199‐ 210.

[E. Tillard, C. H. Moulin, O. Faugère, B. Faugère, 1997](https://agritrop.cirad.fr/388632/). Le suivi individuel des petits ruminants au Sénégal : un mode d’étude des troupeaux en milieu villageois », INRA Prod. Anim., vol. 10, no 1, p. 67‐78.

The other references cited above and a more complete bibliography are available in the manual for the calculation of demographic parameters in tropical livestock herds [here](https://gitlab.cirad.fr/selmet/livtools/laserdemog/-/blob/master/doc/laserdemog_manual%202017-12.pdf)

We highly recommend to have a look to the handbook listed below describing the general theory behind calculation of the demographic parameters, in particular concepts such as probability, hazard rate, demographic interferences or competing risks:

[Lesnoff, M., Lancelot, R., Moulin, C.-H., Messad, S., Juanès, X., Sahut, C., 2014](http://link.springer.com/book/10.1007/978-94-017-9026-0). Calculation of demographic parameters in tropical livestock herds: a discrete time approach with laser animal-based monitoring data. Springer, Dordrecht.

## Support
Please send a message to the maintainer: Xavier Juanes <xavier.juanes@cirad.fr>

## License
[GPL (>= 3)](https://www.gnu.org/licenses/gpl-3.0.html)
